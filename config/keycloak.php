<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lupinus
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.16
 * @link      https://fabrika-klientov.ua
 */

$OIDC_JSON = base_path(env('LUPINUS_OIDC_JSON', 'keycloak.json'));

return file_exists($OIDC_JSON) ? json_decode(file_get_contents($OIDC_JSON), true) : null;
