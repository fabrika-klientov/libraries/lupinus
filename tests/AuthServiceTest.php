<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * Date: 15.02.20
 * Time: 12:43
 */

namespace Tests;

use Lupinus\AuthService;
use Lupinus\Exceptions\LupinusException;
use PHPUnit\Framework\TestCase;

class AuthServiceTest extends TestCase
{

    public function test__construct()
    {
        $this->expectException(LupinusException::class);
        new AuthService();
    }

    public function testAuth()
    {
        $this->assertTrue(true);
    }

    public function testRefresh()
    {
        $this->assertTrue(true);
    }

    public function testCredentials()
    {
        $this->assertTrue(true);
    }
}
