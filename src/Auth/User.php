<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lupinus
 * @category  Lupinus
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.20
 * @link      https://fabrika-klientov.ua
 */

namespace Lupinus\Auth;

/**
 * @property string $sub
 * @property bool $email_verified
 * @property string $user_name
 * @property string $name
 * @property string $preferred_username
 * @property string $given_name
 * @property string $family_name
 *
 * @property array|null $roles (additional) [realmMappings => array, clientMappings => array]
 * .../
 * */
class User implements \JsonSerializable
{
    protected $data;

    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function init(array $data)
    {
        $this->data = $data;
    }

    public function __get($name)
    {
        return $this->data[$name] ?? null;
    }

    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    public function __toString()
    {
        return json_encode($this->data);
    }

    public function jsonSerialize()
    {
        return $this->data;
    }
}
