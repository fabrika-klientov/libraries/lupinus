<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lupinus
 * @category  Lupinus
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.20
 * @link      https://fabrika-klientov.ua
 */

namespace Lupinus\Auth;

use GuzzleHttp\Exception\ClientException;
use Lupinus\Exceptions\LupinusException;
use Lupinus\Services\HttpClient;

class Admin extends User
{
    protected $oidc;
    protected $authData;
    protected static $linkAuth = 'protocol/openid-connect/token';
    private $username;

    /**
     * @param array $oidc
     * @param string $user
     * @param array $data
     */
    public function __construct(array $oidc, string $user, array $data = [])
    {
        parent::__construct($data);
        $this->oidc = $oidc;
        $this->username = $user;
    }

    /**
     * @param array $data
     * @return void
     */
    public function setAuthData(array $data)
    {
        $this->authData = $data;
    }

    /**
     * @param string $link
     * @param array $options
     * @return mixed
     */
    public function get(string $link, array $options = [])
    {
        return $this->request('GET', $link, $this->getAuthHeaders($options));
    }

    /**
     * @param string $link
     * @param array $options
     * @return mixed
     */
    public function post(string $link, array $options = [])
    {
        return $this->request('POST', $link, $this->getAuthHeaders($options));
    }

    /**
     * @param $username
     * @param $password
     * @return array|null
     */
    public function auth($username, $password): ?array
    {
        return $this->queryAuth(
            [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => $this->oidc['resource'],
                    'username' => $username,
                    'password' => $password,
//                    'scope' => '',
                ]
            ]
        );
    }

    /**
     * @param string $refresh_token
     * @return mixed|null
     */
    protected function refreshAuth(string $refresh_token)
    {
        $authData = $this->queryAuth(
            [
                'form_params' => [
                    'grant_type' => 'refresh_token',
                    'client_id' => $this->oidc['resource'],
                    'refresh_token' => $refresh_token,
//                    'scope' => '',
                ]
            ]
        );

        if (empty($authData)) {
            unlink(static::pathAuth($this->username . '.json'));
        }

        return $authData;
    }

    /**
     * @param array $options
     * @return mixed|null
     */
    protected function queryAuth(array $options)
    {
        try {
            $authData = $this->request(
                'POST',
                static::$linkAuth,
                $options,
                true
            );

            if (empty($authData)) {
                return null;
            }

            $this->storeAuthDir();
            file_put_contents(static::pathAuth($this->username . '.json'), json_encode($authData));

            return $authData;
        } catch (\Exception $exception) { // 401 and other
            return null;
        }
    }

    /**
     * @param string $method
     * @param string $link
     * @param array $options
     * @param bool $isStopped
     * @return mixed
     */
    protected function request(string $method, string $link, array $options, bool $isStopped = false)
    {
        try {
            $client = HttpClient::getClient($this->oidc, strpos($link, 'protocol/openid-connect') === false);
            $result = $client->request($method, $link, $options);

            return json_decode($result->getBody()->getContents(), true);
        } catch (ClientException $exception) {
            if ($exception->getCode() == 401 && !$isStopped) {
                $authData = $this->refreshAuth($this->authData['refresh_token'] ?? '');
                $this->setAuthData($authData ?? []);

                return $this->request($method, $link, $this->getAuthHeaders($options), true);
            }

            throw $exception;
        }
    }

    /**
     * @param array $options
     * @return array
     */
    protected function getAuthHeaders(array $options)
    {
        unset($options['headers']['Authorization']);
        return array_merge_recursive(
            $options,
            ['headers' => ['Authorization' => 'Bearer ' . $this->authData['access_token']]]
        );
    }

    /**
     * @param array $oidc
     * @param string|null $username
     * @param string|null $password
     * @return Admin
     * @throws LupinusException
     */
    public static function admin(array $oidc, string $username = null, string $password = null): ?self
    {
        $username = $username ?? env('LUPINUS_AUTH_USERNAME');
        $password = $password ?? env('LUPINUS_AUTH_PASSWORD');

        if (empty($username) || empty($password)) {
            throw new LupinusException('username and password is required');
        }

        if (file_exists(static::pathAuth($username . '.json'))) {
            $authData = json_decode(file_get_contents(static::pathAuth($username . '.json')), true);
            $admin = new self($oidc, $username);
            $admin->setAuthData($authData);

            return $admin;
        }

        $admin = new self($oidc, $username);
        $authData = $admin->auth($username, $password);
        if (empty($authData)) {
            return null;
        }

        $admin->setAuthData($authData);

        return $admin;
    }

    /**
     * @param string|null $file
     * @return string
     */
    protected static function pathAuth(string $file = null): string
    {
        $path = 'lupinus/auth/' . ($file ?? '');
        return function_exists('storage_path') ? storage_path($path) : $path;
    }

    /**
     * @return void
     * */
    protected function storeAuthDir()
    {
        if (!file_exists(static::pathAuth())) {
            if (!mkdir(static::pathAuth(), 0775, true)) {
                throw new \RuntimeException('Did\'t stored path for auth data. Permission denied.');
            }
        }
    }

}
