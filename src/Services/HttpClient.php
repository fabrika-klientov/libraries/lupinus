<?php


namespace Lupinus\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Str;

class HttpClient
{

    /**
     * @param array $oidc
     * @param bool $isAdmin
     * @return Client
     */
    public static function getClient(array $oidc, $isAdmin = false): Client
    {
        $server = Str::afterLast($oidc['auth-server-url'], '/') == 'auth'
            ? ($oidc['auth-server-url'] . '/')
            : $oidc['auth-server-url'];

        return new Client(
            [
                'base_uri' => $server . ($isAdmin ? 'admin/' : '') . 'realms/' . $oidc['realm'] . '/',
                'verify' => false, // delete
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                    'Accept' => 'application/json',
                ],
            ]
        );
    }
}
