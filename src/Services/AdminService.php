<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lupinus
 * @category  Lupinus
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.20
 * @link      https://fabrika-klientov.ua
 */

namespace Lupinus\Services;

use Lupinus\Auth\Admin;
use Lupinus\Auth\User;

class AdminService
{
    private $admin;
    private $oidc;
    protected const LINKS = [
        'roles' => 'users/{user}/role-mappings',
    ];

    /**
     * @param array $oidc
     * @throws \Lupinus\Exceptions\LupinusException
     */
    public function __construct(array $oidc)
    {
        $this->oidc = $oidc;
        $this->admin = Admin::admin($this->oidc);
    }

    /**
     * @param User $user
     * @return array|mixed|null
     */
    public function getRolesForUser(User $user)
    {
        if (isset($this->admin) && empty($user->roles)) {
            $user->roles = $this->admin->get(str_replace('{user}', $user->sub, static::LINKS['roles']));
        }

        return $user->roles;
    }

    // ... other
}
