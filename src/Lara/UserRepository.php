<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lupinus
 * @category  Lupinus
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.20
 * @link      https://fabrika-klientov.ua
 */

namespace Lupinus\Lara;

use Lupinus\Services\AdminService;
use Lupinus\Services\HttpClient;

class UserRepository
{
    private $oidc;
    private $service;
    protected static $userinfo = 'protocol/openid-connect/userinfo';
    protected static $tokenPath = 'protocol/openid-connect/token';

    public function __construct(array $oidc)
    {
        $this->oidc = $oidc;
        $this->service = new AdminService($this->oidc);
    }

    /**
     * @param string $token
     * @param array|null $oidc
     * @return User|null
     */
    public function loadUser(string $token, $oidc = null): ?User
    {
        $oidc = $oidc ?? $this->oidc;
        if (empty($oidc)) {
            return null;
        }

        try {
            $result = json_decode(
                HttpClient::getClient($oidc)->get(
                    static::$userinfo,
                    ['headers' => ['Authorization' => 'Bearer ' . $token]]
                )->getBody()->getContents(),
                true
            );
            if (!empty($result['sub'])) {
                $user = new User($result);
                $this->service->getRolesForUser($user);
                return $user;
            }
        } catch (\Exception $exception) {
        }

        return null;
    }

    /**
     * @param string $code
     * @param string $redirect
     * @return array|null
     */
    public function getToken(string $code, string $redirect): ?array
    {
        try {
            $result = HttpClient::getClient($this->oidc)->post(
                static::$tokenPath,
                [
                    'form_params' => [
                        'code' => $code,
                        'grant_type' => 'authorization_code',
                        'client_id' => $this->oidc['resource'],
                        'redirect_uri' => $redirect,
                    ]
                ]
            );

            return json_decode($result->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return null;
        }
    }

}
