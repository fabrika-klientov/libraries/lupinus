<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lupinus
 * @category  Lupinus
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.20
 * @link      https://fabrika-klientov.ua
 */

namespace Lupinus\Lara;

class Horizon
{

    /**
     * @param User $user
     * @param array $roles
     * @return bool
     */
    public function validate(User $user, $roles)
    {
        return isset($user) &&
            isset($user->roles) &&
            (empty($roles)
                ? true
                : collect($roles)->every(
                    function ($role) use ($user) {
                        return collect($user->roles['realmMappings'])->some('name', $role);
                    }
                )
            );
    }

}
