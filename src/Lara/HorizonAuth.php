<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lupinus
 * @category  Lupinus
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.20
 * @link      https://fabrika-klientov.ua
 */

namespace Lupinus\Lara;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;

/**
 * Middleware for laravel
 * */
class HorizonAuth
{
    protected static $keyToken = 'TOKEN_HORIZON';
    protected $repository;
    private $oidc;
    private $horizonRoles;

    public function __construct()
    {
        $this->oidc = config('keycloak');
        $this->horizonRoles = config('horizon.roles', []);
        $this->repository = new UserRepository($this->oidc ?? []);
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        if ($request->hasCookie(static::$keyToken)) {
            $user = $this->repository->loadUser($request->cookie(static::$keyToken));
            if (isset($user) && Gate::forUser($user)->check('keycloakHorizon', [$this->horizonRoles])) {
                return $next($request);
            }

            if (isset($user)) {
                return response(['status' => false, 'message' => 'Unauthorized.'], 401);
            }
        }

        if (empty($this->oidc)) {
            return response(['status' => false, 'message' => 'Unauthorized.'], 401);
        }

        $server = Str::afterLast($this->oidc['auth-server-url'], '/') == 'auth'
            ? ($this->oidc['auth-server-url'] . '/')
            : $this->oidc['auth-server-url'];
        $redirect = env('APP_URL', '') . '/' . config('horizon.path');
        $redirect = Str::startsWith($redirect, 'http')
            ? $redirect
            : ((env('REDIRECT_HTTPS', true) ? 'https://' : 'http://') . $redirect);

        $keycloakAuth = $server . 'realms/' . $this->oidc['realm']
            . '/protocol/openid-connect/auth?client_id=' . $this->oidc['resource']
            . '&redirect_uri=' . $redirect
            . '&response_type=code&scope=openid';

        $code = $request->input('code');
        if (empty($code)) {
            return redirect()->intended($keycloakAuth);
        }

        $result = $this->repository->getToken($code, $redirect);
        if (empty($result['access_token'])) {
            return redirect()->intended($keycloakAuth);
        }

        $response = $next($request);

        $response->withCookie(cookie()->forever(static::$keyToken, $result['access_token']));

        return $response;
    }

}
