<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lupinus
 * @category  Lupinus
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.15
 * @link      https://fabrika-klientov.ua
 */

namespace Lupinus\Lara;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\UserProvider;

class AccessTokenGuard implements Guard
{
    private $provider;
    private $request;
    private $oidc;
    private $user;
    protected $repository;

    /**
     * @param UserProvider $provider
     * @param $request
     * @param array $config
     */
    public function __construct(UserProvider $provider, $request, array $config)
    {
        $this->provider = $provider;
        $this->request = $request;
        $this->oidc = $config['keycloak'] ?? null;

        if (empty($this->oidc)) {
            \Log::error('Before using keycloak guard - you should setting keycloak config in configs');
        } else {
            $this->repository = new UserRepository($this->oidc);
            $this->loadUser();
        }
    }

    public function check()
    {
        return isset($this->user);
    }

    public function guest()
    {
        // TODO: Implement guest() method.
    }

    public function user()
    {
        return $this->user;
    }

    public function id()
    {
        // TODO: Implement id() method.
    }

    public function validate(array $credentials = [])
    {
        // TODO: Implement validate() method.
    }

    public function setUser(Authenticatable $user)
    {
        $this->user = $user;
    }

    /**
     * @return void
     * */
    protected function loadUser()
    {
        $user = $this->repository->loadUser($this->request->bearerToken());
        if (isset($user)) {
            $this->setUser($user);
        }
    }

}
