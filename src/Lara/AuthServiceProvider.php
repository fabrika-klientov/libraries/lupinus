<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lupinus
 * @category  Lupinus
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.05.20
 * @link      https://fabrika-klientov.ua
 */

namespace Lupinus\Lara;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->publishes(
            [
                __DIR__ . '/../../config/keycloak.php' => config_path('keycloak.php'),
            ],
            'config'
        );
    }

    public function register()
    {
        Auth::extend(
            'keycloak',
            function ($app, $name, array $config) {
                // automatically build the DI, put it as reference
                $userProvider = app(TokenToUserProvider::class);
                $request = app('request');
                $config['keycloak'] = config('keycloak', null);

                return new AccessTokenGuard($userProvider, $request, $config);
            }
        );

        Gate::define('keycloakHorizon', Horizon::class . '@validate');
    }
}
