<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Lupinus
 * @category  Exceptions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.02.15
 * @link      https://fabrika-klientov.ua
 */

namespace Lupinus\Exceptions;

class LupinusException extends \Exception
{
    public function __construct(string $message = "", int $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->setMessage($message);
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message): void
    {
        $this->message = 'LUPINUS::CORE: ' . $message;
    }
}