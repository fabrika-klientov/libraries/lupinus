## Status

[![Latest Stable Version](https://poser.pugx.org/shadoll/lupinus/v/stable)](https://packagist.org/packages/shadoll/lupinus)
[![pipeline status](https://gitlab.com/fabrika-klientov/libraries/lupinus/badges/master/pipeline.svg)](https://gitlab.com/fabrika-klientov/libraries/lupinus/commits/master)
[![License](https://poser.pugx.org/shadoll/lupinus/license)](https://packagist.org/packages/shadoll/lupinus)

---

[doc](https://www.keycloak.org/docs-api/10.0/rest-api/index.html)

[doc](https://github.com/keycloak/keycloak-documentation/blob/master/securing_apps/topics/oidc/oidc-generic.adoc#_token_introspection_endpoint)

[doc](https://www.keycloak.org/docs/latest/securing_apps/#_javascript_adapter)

[doc](https://openid.net/specs/openid-connect-core-1_0.html#AuthorizationEndpoint)

Настройка (для laravel)

## Закрытие Api запросов  
1. Подключить провайдер 

```php
    // config/app.php

    /*
     * Application Service Providers...
     */
    // ...
    Lupinus\Lara\AuthServiceProvider::class,
    
```

2. Опубликовать конфиг файл 
`php artisan vendor:publish  --provider="Lupinus\Lara\AuthServiceProvider"`

    - Настроить файл (это сводится к копированию `OIDC` таблицы (в json)
 с сервера `keycloak`) в переменную `$OIDC_JSON`

3. В конфиге `config/auth.php` 
```php
    // config/auth.php

    'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],

        'api' => [
            'driver' => 'keycloak', // <- set to keycloak
            'provider' => 'users',
            'hash' => false,
        ],
    ],
```

4. Добавить в нужный роут middleware 

```php

    Route::prefix('api/v1')
        ->middleware('auth:api') // <-
        ->...;
```

## Admin keycloak

Для работы с keycloak в роли администратора необходимо
 определить переменные окружения `LUPINUS_AUTH_USERNAME`
  и `LUPINUS_AUTH_PASSWORD`
  
Для `Закрытие Api запросов` admin не нужен
 (только в случае если понадобятся роли)


## Закрытие Horizon

**Необходим `Admin keycloak`**

1. В `App\Http\Kernel` в секцию `$routeMiddleware` добавить 
    `'horizon.auth' => \Lupinus\Lara\HorizonAuth::class,`

2. В конфиг файле `horizon.php` добавить

```php

    'middleware' => ['web', 'horizon.auth'], // <- к существующему web добавить horizon.auth

    'roles' => ['horizon'], // <- добавить роли (по необходимости)
```

3. В `App\Providers\HorizonServiceProvider` переопределить метод `authorization`:
(так как авторизация будет проводится через Middleware)

```php
    // ...

    protected function authorization()
    {
        Horizon::auth(function ($request) {
            return true;
        });
    }

    // ...
```

